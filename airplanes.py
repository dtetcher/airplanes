from random import sample
from typing import List, Dict
from operator import itemgetter
from os import system, name
from tkinter import *
import tkinter.font as tkfont
import tkinter.messagebox as mb


class Airplane:
    # Simple constructor. Calls when you create object of class
    # This constructor will automatically generate record for you
    def __init__(self):
        self.__db = Airplane.__generate()

    # if you won't specify any index it will return
    # all record, in other case record element at index
    def getDB(self, index: int = None):
        """
        :param index:array element index
        :return:Attribute of record
        """
        if index is None:  # if index not set -- return db
            return self.__db
        return self.__db[index]  # if index set returns element at specified index

    # changes db element at 'index' and replaces its value on 'value'
    def setDB(self, index: int, value) -> None:
        """
        :param index: array element index
        :param value: replacement
        """
        _type = type(self.__db[index])
        try:
            self.__db[index] = _type(value)
        except ValueError as ve:
            print(f"\nIncompatible data type. Element {index} requires {_type}.\n{str(ve)}\n")

    # compares object's field(by 'field' parameter) which call method with object which
    # specified as parameter
    def compare(self, obj, field: int = 4) -> int:
        """
        :param obj: Instance of Airplane class
        :param field: Field for comparison (default: max speed)
        :return: int: 0 if equal, 1 if caller bigger, -1 if lesser
        """
        if not isinstance(obj, Airplane):
            raise TypeError
        return 1 if self.__db[field] > obj.__db[field] else 0 if self.__db[field] == obj.__db[field] else -1

    # Clears our db
    def clear(self) -> None:
        """
        Clears database
        """
        self.__db.clear()

    # Replace generated db with custom
    def load(self, record: list) -> None:
        """
        Rewrites object's list
        :param record: Replacement
        """
        # self.clear()
        self.__db = record

    # method can be accessed without it class object creation. 'AirplaneController.generate()' like this.
    @staticmethod   # __ - private (visible only in class boundaries)
    def __generate() -> list:
        # simple hand generated database
        db = [["Jason Burn", "Nathaniel Gier", "Sydney Wallet", "Jasini Ziro"],  # manufacturers
              ["A", "B", "C", "D"],  # models
              [2005, 2001, 1014, 2019],  # years
              [1000, 2030, 140, 240],  # passengers
              [120, 232, 142, 425]]  # max speed
        # making fork db from one which above, i.e if size = 5 db_set=[[], [], [], [], []]
        # db_set = [[] for i in range(size)]  # each nested array is a record
        record: list = list()
        # for record in range(size):  # record from db set
        for field in range(len(db)):  # fields in db (manufacturers, models, years, passengers)
            data_type = type(db[field][0])  # it will read type of every first element in our db i.e Jason Burn, A..
            # returns random element from each field(in 'list' data type)
            # i.e random from models, because we randomly took only one elem, we can refer to this element
            # and cast it to our data type
            value = data_type(sample(db[field], 1)[0])
            record.append(value)  # push it to our record
        return record


class AirplaneController:
    def __init__(self):
        self.__db: List[list] = list()  # initiate empty list, which will contain lists(records) inside it
        self.__record_length: int = 5  # initiate our record default length

    def lastElemId(self) -> int:
        return len(self.__db) - 1

    def getRecordLength(self):  # record_length's getter and setter
        return self.__record_length

    def setRecordLength(self, value: int):
        self.__record_length = value

    def setDB(self, value: list, index: int = None):
        if len(value) != self.__record_length:
            raise BufferError
        if index is None:
            self.__db.append(value)
            return
        self.__db[index] = value

    def getDB(self, index: int = None):
        if index is None:
            return self.__db
        return self.__db[index]

    # Property for more comfortable usage
    # In this way we will be able to set variable 'this_class_object.sentence_length = 7' like this
    # and get 'var x = this_class_object.sentence_length' like this. Instead of calling getter and setter
    record_length = property(getRecordLength, setRecordLength)

    #
    # FUNCTIONS FOR OPERATIONS OVER __db
    #                   # takes zero and more objects of class Airplane (varargs)
    def append(self, *obj_args: Airplane) -> None:
        for obj in obj_args:    # iterates over specified objects
            # if object's record length is not equal to our record length then spawn exception
            if len(obj.getDB()) != self.__record_length:
                print(f"Incorrect list length in {obj}. Must be {self.__record_length}.")
                raise BufferError
            self.__db.append(obj.getDB())   # append object's record to our db

    def clear(self) -> None:    # clears our db
        self.__db.clear()

    #
    #   FUNCTIONS FOR TASK OPERATIONS
    #
    # two underscores(__) means it's private method and visible only within current class
    def start(self):    # CLASS MAIN METHOD
        while True:
            self.__menu()  # prints menu each time loop starts
            # makes call to our switch passes our db and function number
            try:
                self.__switch(int(input("--->>  ")))
            except ValueError:
                continue

    # its kind of method overloading we can call this method like 'print()' and it will print all database.
    # and we can call it like this 'print(attr=True)' or 'print(True)' and it will print only records by specified field
    def __print(self, attr: bool = False) -> None:
        if attr is False:   # print whole db(when attr is not set)
            keyword = ["Manufacturer", "Model", "Year", "Passengers", "Max speed"]
            for record in self.__db:
                for field in range(len(record)):
                    print(f"{keyword[int(field)]}: {record[int(field)]}")
                print()
            AirplaneController.cleaner()  # our static cleaner
        else:       # 'print by field' mode
            field: str = input("Enter attribute:")
            if len(field) == 0:
                print("\nNo input detected\n")
                return
            for record in self.__db:
                for record_value in record:
                    _type = type(record_value)
                    if record_value == _type(field):
                        print(record)
            AirplaneController.cleaner()

    def __append(self):     # appends record to db
        try:
            self.__db.append([input("Manufacturer: "), input("Model: "),
                              int(input("Year: ")), int(input("Passengers: ")),
                              int(input("Max speed: "))])
        except ValueError:  # if block above throws this exceptions it handles below
            print("\nSome of fields are empty\n")
        AirplaneController.cleaner()

    def __sort(self) -> None:   # sorts db
        try:
            field: int = int(input("Select field\n1.Manufacturer\n2.Model\n3.Year\n4.Passengers\n5.Max speed\n--->> ")) - 1
        except ValueError:  # this exception is thrown when you specify wrong data for datatype i.e empty string for int
            print("\nNo input detected\n")
            return
        self.__db.sort(key=itemgetter(field))  # sorts db by specified field above
        AirplaneController.cleaner()

    # same as with __print. WORKS ONLY WITH ONE VARIABLE SET
    def __remove(self, idx: bool = False, attr: bool = False) -> None:
        if idx is True and attr is False:  # BLOCK IMPLEMENTS removing by index
            try:
                idx: int = int(input("Enter record ID: "))
            except ValueError:
                print("No input detected")
                return
            self.__db.pop(idx)
            AirplaneController.cleaner()
        elif attr is True and idx is False:  # BLOCK IMPLEMENTS removing by field
            try:
                fieldId: int = int(
                    input("Enter field:\n1.Manufacturer\n2.Model\n3.Year\n4.Passenger\n5.Max speed\n--->> ")) - 1
                match = input("Enter desirable match: ")
            except ValueError:
                print("Some of fields are empty")
                return
            for record in self.__db:
                data_type = type(record[fieldId])
                if record[fieldId] == data_type(match):
                    self.__db.remove(record)
                    AirplaneController.cleaner()
        else:
            print("Specify one parameter")
            AirplaneController.cleaner()

    @staticmethod
    def __menu() -> None:   # menu as static method
        sep_size: int = 30
        print(f"***MENU***\n{'-' * sep_size}")
        print("1. Print list\n2. Add record\n3. Sort by field")
        print("4. Remove by field\n5. Remove by index\n6. Print by field")
        print('-' * sep_size)

    def __switch(self, case: int):
        switcher = {1: self.__print,  # dictionary key->value(input from menu -> function name)
                    2: self.__append,  # simply maps nums to functions
                    3: self.__sort,
                    4: self.__remove,
                    5: self.__remove,
                    6: self.__print
                    }

        func = switcher.get(case)
        if case in range(1, 4):     # applies to functions with no params(case 1,2,3)
            func()
        elif case == 4:     # remove by attribute
            func(attr=True)
        elif case == 5:
            # remove by index. Doesn't work properly if call it without referring to current object. idk why
            self.__remove(idx=True)
            # func(idx=True)
        elif case == 6:
            func(attr=True)

    @staticmethod
    def cleaner() -> None:  # simple console screen cleaner
        print("\n\nPress ENTER")
        while True:  # infinite loop
            if input() == "":  # while enter not pressed
                break
        # if system runs on NT kernel choose 'cls' command in other way (for Linux of MacOS) 'clear')
        if name == 'nt':
            system('cls')
        else:
            system('clear')


# Intermediate class between controller and gui
class AirplaneConfig:
    # receives AirplaneController object and dict (keyword->requirable datatype for this field)
    def __init__(self, controller: AirplaneController, keywords: dict):
        # maps controller records to len and iterates through it
        for record_length in map(len, controller.getDB()):
            # if all records not the same size it will throw exception
            if record_length != len(controller.getDB(0)):
                raise BufferError('Controller field\'s length are not equal')
        # in other case it will get length of first db record
        self.record_length = len(controller.getDB(0))

        # validates if dict's keys are 'str' data type, and value's type is instance of class 'type'
        for key, value in keywords.items():
            if type(key) is not str or type(value) is not type:
                raise TypeError("Incorrect types given. Expected <class str>, <class type>")

        # Checks wheather keyword dict length is equal to our record length
        if len(keywords) != self.record_length:
            raise BufferError('Keywords size must be the same as record\'s')
        # gets keys and casts to list and saves as instance variable
        self.keywords = list(keywords.keys())
        # gets values and casts to list and saves as instance variable
        self.data_types = list(keywords.values())
        # Saves controller, so in the end we will give only instance of this class as param
        # to AirplaneGUI class
        self.controller = controller


class AirplaneGUI:
    def __init__(self, config: AirplaneConfig):
        # Creates our root window
        self.__window = Tk()
        # gets db from controller
        self.__db = config.controller.getDB()
        # initializes home page in constructor
        self.__initHome()
        # get configs
        self.__conf = config
        # sets default header's font
        self.font = tkfont.Font(family="Lucida Grande", size=20)

    # simple method which runs our root window
    def run(self) -> None:
        self.__window.mainloop()

    def __initHome(self):
        self.__window.title('Airplanes Manager')    # window title
        # math manipulation to spawn window in the middle of the screen
        width, height = 850, 500
        padding_x: int = self.__window.winfo_screenwidth()/2 - (width/2)
        padding_y: int = self.__window.winfo_screenheight()/2 - (height/2)
        self.__window.geometry(f"{width}x{height}+{int(padding_x)}+{int(padding_y)}")
        # Sets welcome message on home page
        Label(self.__window, text='Welcome to', font=("Arial Bold", 26)).pack()
        Label(self.__window, text="\"Airplane Manager\"", font=("Helvetica", 22)).pack()

        # creates menu
        menu_bar = Menu(self.__window)
        self.__window.config(menu=menu_bar)
        # if in labels dict is nested dict then program will treat it like cascade menu
        # (Key of nested dict is cascade's name)
        labels = {"New": self.__append, 'Sort': self.__sort,
                  "Print": {"Print all": self.__print, "Print by field": self.__print_by_field},
                  "Remove": {"Remove by value": self.__remove_by_field, "Remove by index": self.__remove_by_index}
                  }

        for label, command in labels.items():  # iterates over list elements
            # if value of element is dict then it treats it like cascade menu
            # i.e third element
            if type(command) is dict:
                # creates new menu for cascade
                sub_bar = Menu(menu_bar)
                # in case of third element it will iterate through its value dict
                # nested_lbsl will be "Print all" and cmd - self.__print and so on..
                for nested_lbl, nested_cmd in command.items():
                    # adds command to temporary menu
                    sub_bar.add_command(label=nested_lbl, command=nested_cmd)
                    # adds separator for better view
                    sub_bar.add_separator()
                # and then when nested dict is over, command below adds temp menu to our main as cascade
                menu_bar.add_cascade(label=label, menu=sub_bar)
                continue    # continues to the next iteration son next line will not execute
            # adds command if value of element is not dict
            menu_bar.add_command(label=label, command=command)
        # saves all changes in menu to our root window
        self.__window.config(menu=menu_bar)

    # It is necessary to avoid functions receiving additional params.
    # 'Cause when we use function in dict, adding of () will automatically make call to
    # this function.
    def __print_by_field(self):
        self.__print(attr=True)

    def __remove_by_field(self):
        self.__remove(attr=True)

    def __remove_by_index(self):
        self.__remove(idx=True)

    # No need for 'append' and 'sort' redeclaration, 'cause it takes only one param

    def __clean(self):
        # this function calls on each function start
        # it clears all root window elements except our menu
        for window in self.__window.winfo_children():
            if window.__str__() != ".!menu":
                window.destroy()

    def __print(self, attr: bool = False) -> None:
        self.__clean()
        #
        # NOTICE!!! ALL THIS BLOCK UPON 'if attr is False:' EXECUTES NO MATTER WHICH PARAMETER IS SET
        #

        # because of this pattern we will be able to print db in beautiful way
        # with padding in 20 characters
        padding_size = 20
        pattern = "{:<" + str(padding_size) + "} "
        # creates variable to further usage, its where our printed db will save
        text = Text(self.__window, wrap='word')
        scrollbar = Scrollbar(text) # adds scrollbar

        # save all changes to root window
        text.configure(yscrollcommand=scrollbar.set)
        scrollbar.config(command=text.yview)
        scrollbar.pack(side=RIGHT, fill=Y)
        text.pack(side=LEFT, expand='yes', fill="both")

        # adds to our text header (Manuf, model, year, and so on)
        for keyword in self.__conf.keywords:
            # formats our text by our pattern, and inserts it at at the end of content
            text.insert(END, pattern.format(keyword))
        text.insert(END, f"\n{'-'*100}\n")

        if attr is False:  # print whole db(when attr is not set)
            self.__window.geometry("980x420") # sets custom geometry for root

            # iterates through record's fields and print it by pattern
            for record in self.__db:
                for field in record:
                    text.insert(END, pattern.format(field))
                text.insert(END, "\n")
            text.see(END)   # visualizing text

        else:  # 'print by field' mode

            self.__window.geometry("1200x420")

            Label(self.__window, text="Enter attribute", padx=5, pady=5).pack(anchor=CENTER)
            entry = Entry(self.__window, bd=5)  # entry for our field
            entry.focus()   # focused by default
            entry.pack()    # packs in root window

            def _event():   # predeclared nested function below, so we can see all executable commands
                event()

            Button(self.__window, text="Search", command=_event).pack()

            def event():
                notFound = True
                field = entry.get()     # gets text from input entry

                if len(field) == 0:     # if nothing entered
                    mb.showerror("Error", "Field is empty")
                    return

                for record in self.__db:
                    for record_field in record:
                        _type = type(record_field)
                        try:
                            if record_field == _type(field):
                                # if execpt one time it will pass line above then matches found
                                notFound = False
                                for f in record:
                                    text.insert(END, pattern.format(f))
                                text.insert(END, "\n")
                        # if while casting our field to db field type exception is thrown, then skip iteration
                        except ValueError:
                            continue
                if notFound:    # if nothing found by match
                    mb.showinfo("Search result", "No matches found")
                    return
                text.see(END)

    def __append(self):  # appends record to db
        self.__clean()

        Label(self.__window, text="Input data", padx=5, pady=5, font=self.font).pack(anchor=CENTER)
        entries: list = []  # array for our entries
        # creates entry for each record's field
        for kword in self.__conf.keywords:
            Label(self.__window, text=kword).pack()
            entry = Entry(self.__window, bd=5)
            entry.pack()
            entries.append(entry)

        def _event():
            event()

        Button(self.__window, text="Register", command=_event).pack()

        def event():
            values = list()
            try:
                # using enumerate we will be able to use indexes of element
                for i, entry in enumerate(entries):
                    _type = self.__conf.data_types[i]   # gets desirable data type from our conf
                    values.append(_type(entry.get()))   # appends casted to _type input from entry
                self.__db.append(values)
                # if exeception not thrown, then prints who was added
                mb.showinfo("Success", f"'{entries[0].get()}' added to database")
            except ValueError:  # if block above throws this exceptions it handles below
                mb.showerror("Error", "Some fields are empty, or incorrect data types given.")

    def __sort(self) -> None:  # sorts db
        self.__clean()

        Label(self.__window, text="Sort by:", padx=5, pady=5, font=self.font).pack()
        choice = IntVar()   # variable where our radiobutton choice will save

        # function is tiny so no need for predeclaration
        def _event():
            self.__db.sort(key=itemgetter(choice.get()))  # sorts db by specified field above
            mb.showinfo("Success", f"Database sorted by {self.__conf.keywords[choice.get()]} field")

        for i, kword in enumerate(self.__conf.keywords):    # creates radiobuttons
            # variable is where our selected button's value will save
            # value its keyword position in list
            Radiobutton(self.__window, text=kword, variable=choice, value=i).pack(anchor=CENTER)

        Button(self.__window, text="Sort", command=_event).pack()

    # same as with __print. WORKS ONLY WITH ONE PARAMETER SET
    def __remove(self, idx: bool = False, attr: bool = False) -> None:
        self.__clean()

        if idx is True and attr is False:  # BLOCK IMPLEMENTS removing by index

            Label(self.__window, text="Enter record ID", padx=5, pady=5, font=self.font).pack()
            index = Entry(self.__window, bd=5)
            index.pack()

            def _event():
                try:
                    self.__db.pop(int(index.get()))
                    mb.showinfo("Success", f"Record {index.get()} removed")
                except ValueError:
                    # throws if casting to int is unsuccessful
                    mb.showerror("Error", "Incorrect input")
                    return
                except IndexError:
                    # throws if specified index is out of db bounds
                    mb.showerror("Error", "No record with such ID")
                    return

            Button(self.__window, text="Remove", command=_event).pack()

        elif attr is True and idx is False:  # BLOCK IMPLEMENTS removing by field

            Label(self.__window, text="Remove by:", padx=5, pady=5, font=self.font).pack(anchor=NW)
            choice = IntVar()

            matcher = Entry(self.__window, bd=5)
            matcher.pack()

            for i, kword in enumerate(self.__conf.keywords):
                Radiobutton(self.__window, text=kword, variable=choice, value=i).pack(anchor=NW)

            def _event():
                event()

            Button(self.__window, text="Remove", command=_event).pack()

            def event():
                if len(matcher.get()) == 0: # if nothing entered
                    mb.showerror("Error", "No input specified")
                    return
                counter: int = 0    # counts how much records removed
                for record in self.__db:
                    data_type = self.__conf.data_types[choice.get()]    # field's required data type
                    if record[choice.get()] == data_type(matcher.get()):    # if match found
                        self.__db.remove(record)    # remove record
                        counter += 1    # iterate counter
                mb.showinfo("Success", f"{counter} record(s) removed")


if __name__ == "__main__":
    obj1 = Airplane()
    obj2 = Airplane()
    obj3 = Airplane()
    obj4 = Airplane()
    obj5 = Airplane()

    controller = AirplaneController()
    controller.append(obj1, obj2, obj3, obj4, obj5)

    kwords: dict = {"Manufacturer": str, "Model": str, "Year": int, "Passengers": int, "Max speed": int}
    conf = AirplaneConfig(controller, kwords)

    gui = AirplaneGUI(conf)
    gui.run()
